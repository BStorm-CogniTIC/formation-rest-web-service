Rails.application.routes.draw do
  mount SwaggerEngine::Engine, at: "/doc"

  namespace :api do
    resources :veterinaires, except: [:new]
    resources :produits, except: [:new]
    resources :prescriptions, except: [:new]
    
    get 'reset', to: 'database#reset'
  end
end
