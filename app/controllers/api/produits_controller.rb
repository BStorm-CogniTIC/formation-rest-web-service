class Api::ProduitsController < ApplicationController
  def index
    @produits = Produit.all
    render json: @produits
  end

  def show
    @produit = Produit.find(params[:id])
    render json: @produit
  end

  def create
    @produit = Produit.new(produit_params)
    if @produit.save
      render json: @produit, status: :created
    else
      render json: @produit.errors, status: :unprocessable_entity
    end
  end

  def update
    @produit = Produit.find(params[:id])
    if @produit.update(produit_params)
      render json: @produit, status: :accepted
    else
      render json: @produit.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @produit = Produit.find(params[:id])
    @produit.destroy()
    render nothing: true, status: :no_content
  end

  private
    def produit_params
      params.required(:produit).permit(:nom)
    end
end
