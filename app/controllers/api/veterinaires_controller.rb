class Api::VeterinairesController < ApplicationController
  def index
    @vetos = Veterinaire.all
    render json: @vetos
  end

  def show
    @veto = Veterinaire.find(params[:id])
    render json: @veto
  end

  def create
    @veto = Veterinaire.new(veto_params)
    if @veto.save
      render json: @veto
    else
      render json: @veto.errors, status: :unprocessable_entity
    end
  end

  def update
    @veto = Veterinaire.find(params[:id])
    if @veto.update(veto_params)
      render json: @veto
    else
      render json: @veto.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @veto = Veterinaire.find(params[:id])
    @veto.destroy()
    render nothing: true, status: :no_content
  end

  private
    def veto_params
      params.required(:veterinaire).permit(:nom)
    end
end
