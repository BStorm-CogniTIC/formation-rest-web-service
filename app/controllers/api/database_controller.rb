require 'rake'
Rake::Task.clear
WebService::Application.load_tasks

class Api::DatabaseController < ApplicationController
  def reset
    Rake::Task['db:reset'].invoke
    Rake::Task['db:seed'].invoke
  end
end
