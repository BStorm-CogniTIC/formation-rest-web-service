class Api::PrescriptionsController < ApplicationController
  def index
    @prescriptions = Prescription.all
    render json: @prescriptions
  end

  def show
    @prescription = Prescription.find(params[:id])
    render json: @prescription
  end

  def create
    @prescription = Prescription.new(prescription_params)
    if @prescription.save
      render json: @prescription, status: :created
    else
      render json: @prescription.errors, status: :unprocessable_entity
    end
  end

  def update
    @prescription = Prescription.find(params[:id])
    if @prescription.update(prescription_params)
      render json: @prescription, status: :accepted
    else
      render json: @prescription.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @prescription = Prescription.find(params[:id])
    @prescription.destroy()
    render nothing: true, status: :no_content
  end

  private
    def prescription_params
      params.required(:prescription).permit(:veterinaire_id, {produit_ids: []})
    end
end
