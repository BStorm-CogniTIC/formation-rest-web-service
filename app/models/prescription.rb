class Prescription < ActiveRecord::Base
  belongs_to :veterinaire
  has_and_belongs_to_many :produits
  accepts_nested_attributes_for :produits

  validates :veterinaire, presence: true
end
