class Veterinaire < ActiveRecord::Base
  has_many :prescriptions, inverse_of: :veterinaire, dependent: :destroy

  validates :nom, presence: true, length: { maximum: 255}
  validates_associated :prescriptions
end
