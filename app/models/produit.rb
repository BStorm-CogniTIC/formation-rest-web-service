class Produit < ActiveRecord::Base
  has_and_belongs_to_many :prescriptions

  validates :nom, presence: true, length: { maximum: 255}
end
