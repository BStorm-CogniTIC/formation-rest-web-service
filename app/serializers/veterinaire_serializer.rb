class VeterinaireSerializer < ActiveModel::Serializer
  attributes :id, :nom, :prescriptions_count

  def prescriptions_count
    object.prescriptions.count
  end
end
