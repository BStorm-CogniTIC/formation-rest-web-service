class PrescriptionSerializer < ActiveModel::Serializer
  attributes :id

  belongs_to :veterinaire
  has_many :produits
end
