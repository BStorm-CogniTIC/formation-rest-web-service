# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

vetos = Veterinaire.create([{nom: 'Erica Riley'}, {nom: 'Russell Woods'}, {nom: 'Jack Ramirez'}])
produits = Produit.create([{nom: 'Advanthome Spray'}, {nom: 'Cerumaural'}])
Prescription.create([{veterinaire: vetos.first, produits: produits}, {veterinaire: vetos.last, produits: [produits.first]}])