class CreateVeterinaires < ActiveRecord::Migration
  def change
    create_table :veterinaires do |t|
      t.string :nom, null: false

      t.timestamps null: false
    end
  end
end
