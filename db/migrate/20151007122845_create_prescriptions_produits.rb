class CreatePrescriptionsProduits < ActiveRecord::Migration
  def change
    create_table :prescriptions_produits do |t|
      t.integer :prescription_id, null: false
      t.integer :produit_id, null: false
    end
    add_index :prescriptions_produits, [:prescription_id, :produit_id]
  end
end
