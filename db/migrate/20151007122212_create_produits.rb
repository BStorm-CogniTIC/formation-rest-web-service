class CreateProduits < ActiveRecord::Migration
  def change
    create_table :produits do |t|
      t.string :nom, null: false

      t.timestamps null: false
    end
  end
end
