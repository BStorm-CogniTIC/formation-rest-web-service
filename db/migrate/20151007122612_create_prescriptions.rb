class CreatePrescriptions < ActiveRecord::Migration
  def change
    create_table :prescriptions do |t|
      t.references :veterinaire, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
