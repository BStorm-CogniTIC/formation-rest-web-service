# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151007122845) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "prescriptions", force: :cascade do |t|
    t.integer  "veterinaire_id", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "prescriptions", ["veterinaire_id"], name: "index_prescriptions_on_veterinaire_id", using: :btree

  create_table "prescriptions_produits", force: :cascade do |t|
    t.integer "prescription_id", null: false
    t.integer "produit_id",      null: false
  end

  add_index "prescriptions_produits", ["prescription_id", "produit_id"], name: "index_prescriptions_produits_on_prescription_id_and_produit_id", using: :btree

  create_table "produits", force: :cascade do |t|
    t.string   "nom",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "veterinaires", force: :cascade do |t|
    t.string   "nom",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "prescriptions", "veterinaires"
end
