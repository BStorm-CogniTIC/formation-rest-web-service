# Web service

> Réaliser pour les exercices lors de formation

## Informations générales

- Toutes les URL pour interagir en REST doivent commencer par `/api`
- Une URL de reset est présente (reset de la DB et ajout de données de départ): `/api/reset`
- La documentation se trouve à l'URL suivante : `/doc`

## Hébergement

Ce web service est sur Heroku à l'adresse suivante : `https://ex-ajax.herokuapp.com`